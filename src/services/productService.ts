import { Knex } from 'knex'

import productRepository, { Product } from '../repository/productRepository'

export async function getProducts (db: Knex): Promise<Product[]> {
  const products = await productRepository.findAll(db)

  return products
}

export async function getDetail (db: Knex, id: string): Promise<Product | undefined> {
  return await productRepository.detail(db, id)
}