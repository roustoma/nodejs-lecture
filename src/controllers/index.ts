import Router from 'koa-router'

import { AppContext, AppState } from '../app'
import statusRouter from './status'
import productRouter from './product'

const apiRouter = new Router<AppState, AppContext>({
  prefix: '/api/v1',
})

apiRouter
  .use('/status', statusRouter)
  .use('/products', productRouter)

export default apiRouter.routes()