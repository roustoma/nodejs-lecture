
import Router from 'koa-router'

import { AppContext, AppState } from '../app'

import { getProducts, getDetail } from '../services/productService'

const router = new Router<AppState, AppContext>()

router.get('/', async (ctx) => {
  const products = await getProducts(ctx.db)

  ctx.body = products
  ctx.status = 200
})

router.get('/:id', async (ctx) => {
  const product = await getDetail(ctx.db, ctx.params.id)

  if (!product) { // Usually not used this way. It's just for teaching purpose
    ctx.body = 'Not found'
    ctx.status = 404
  } else {
    ctx.body = product
    ctx.status = 200
  }
})

export default router.routes()