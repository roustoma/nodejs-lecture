import { Knex } from 'knex'
import Koa, { Context } from 'koa'
import bodyParser from 'koa-bodyparser'

import routes from './controllers'
import initDatabase from './database'
import errorMiddleware from './middleware/errorMiddleware'

export type AppState = {}

export interface AppContext extends Context {
  db: Knex
}

export default (): Koa<AppState, AppContext> => {
  const app = new Koa<AppState, AppContext>()

  app.context.db = initDatabase()

  app
    .use(errorMiddleware)
    .use(bodyParser())
    .use(routes)

  return app
}

