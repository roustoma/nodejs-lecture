import { Knex } from "knex"

import { schema } from '../database/schema'

export interface Product {
  id: string
  title: string
  price: number
  description: string
  category: string
  image: string
}


export default {
  async findAll(db: Knex): Promise<Product[]> {
    const products = await db(schema.products.name).select<Product[]>('*')

    return products
  },

  async detail(db: Knex, id: string) {
    const product = await db(schema.products.name)
      .select<Product>('*')
      .where('id', id)
      .first()

    return product
  },
}