import { Knex, knex } from 'knex'

const config: Knex.Config = {
   client: 'pg',
   connection: {
     host: 'localhost',
     database: 'mini-eshop',
     user: 'postgres',
     password: 'postgres'
   }
}

export default (): Knex => {
  return knex(config)
}