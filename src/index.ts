import app from './app'
import startServer from './server'


const PORT = 8080

function bootstrap () {
  startServer(app(), PORT).catch((err: Error) => {
    console.log(`Failed to start HTTP server: ${err.message}`)
  })
}

bootstrap()