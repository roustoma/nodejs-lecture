import { Next } from "koa"
import { AppContext } from "../app"

export default async function (ctx: AppContext, next: Next): Promise<void> {
  try {
    await next()
  } catch (err) {
    ctx.status = 500
    ctx.body = {
      title: 'Chyba na serveru',
      status: 500
    }

    ctx.app.emit('error', err, ctx)
  }
}