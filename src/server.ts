import * as http from 'http'
import Koa from 'koa'


export default async (app: Koa<any, any>, port: number): Promise<http.Server> => {
  return new Promise((resolve, reject) => {
    const server = http.createServer(app.callback())

    server.on('listening', () => {
      console.log('Server listens on port: ', port)
      resolve(server)
    })

    server.on('error', reject)

    server.listen(port)
  })
}
